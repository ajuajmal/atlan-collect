. set_env.sh

docker-compose -f stage.yml run django python manage.py migrate

docker-compose -f stage.yml run django python manage.py collectstatic --noinput

docker-compose -f stage.yml exec postgres backup
docker-compose -f stage.yml run --rm awscli upload

docker image prune -f
docker volume prune -f
docker container prune -f
