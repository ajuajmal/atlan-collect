export DATABASE_URL="sqlite:///db.sqlite"
export CELERY_BROKER_URL=redis://localhost:6379/0
export USE_DOCKER=False
