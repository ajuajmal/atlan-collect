ssh -o StrictHostKeyChecking=no $CONNECT_DEPLOY_INSTANCE << 'ENDSSH'
  cd atlan
  export $(cat ./.envs/.production/.django | xargs)
  docker login -u $CI_DOCKER_DEPLOY_USER -p $CI_DOCKER_DEPLOY_KEY $CI_REGISTRY
  docker pull $IMAGE:docs-stage || true
  docker pull $IMAGE:django-stage
  docker pull $IMAGE:postgres-stage
  docker pull $IMAGE:flower-stage || true
  docker pull $IMAGE:celeryworker-stage || true
  docker pull $IMAGE:celerybeat-stage || true
  docker pull $IMAGE:traefik-stage
  docker-compose -f stage.yml up -d
  . setup.sh
ENDSSH
