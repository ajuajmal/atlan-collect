from django.contrib.auth import get_user_model

from config import celery_app
import time
User = get_user_model()


@celery_app.task()
def get_users_count():
    """A pointless Celery task to demonstrate usage."""
    return User.objects.count()

@celery_app.task(name="sum_two_numbers",bind=True,soft_time_limit=1001)
def add2(self,x, y):
    #print(celery_app.current_task.task_id)
    print('Executing task id {0.id}, args: {0.args!r} kwargs: {0.kwargs!r}'.format(
            self.request))
    print('start sleep')
    time.sleep(1000)
    print('end sleep')
    return x + y

@celery_app.task(name="working process",bind=True,soft_time_limit=1001)
def long_test(self, i):
    print('long test starting with delay of ' + str(i) + 'seconds on each loop')
    print('task_id =' + str(self.request.id))
    print(self)
    self.update_state(state='PROCESSING')
    count = 0
    while True:
        task = celery_app.AsyncResult(self.request.id)
        while task.state == 'PAUSING' or task.state == 'PAUSED':
            if task.state == 'PAUSING':
                self.update_state(state='PAUSED')
            time.sleep(i)
        if task.state == 'RESUME':
            self.update_state(state='PROCESSING')
        print('long test loop ' + str(count) + ' ' + str(task.state))
        count += 1
        time.sleep(i)
