from django.conf import settings
from django.db import models
from django.core.validators import FileExtensionValidator
from django.conf import settings
from django.db import models
from django.core.validators import FileExtensionValidator
import os
from django.utils import timezone
from datetime import datetime
from django.core.serializers.json import DjangoJSONEncoder
from django.core.files.uploadedfile import InMemoryUploadedFile

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import json

def file_uploads_directory_path(instance, filename):
    basefilename, file_extension = os.path.splitext(filename)
    timenow = timezone.now()
    return 'file/{user}/{time}/{basename}{time}{ext}'.format(user=instance.user.username, basename=basefilename, time=timenow.strftime("%Y%m%d%H%M%S"), ext=file_extension)

class FileJsonEncoder(DjangoJSONEncoder):
    def default(self, o):
        if isinstance(o, InMemoryUploadedFile):
           return o.read()
        return str(o)

class BaselineUpload(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.CharField(blank=True,max_length=70)
    file = models.FileField(upload_to=file_uploads_directory_path, validators=[
                            FileExtensionValidator(allowed_extensions=['csv'])], default='test.csv', help_text="TestFiles")
    task_id = models.CharField(blank=True,max_length=36)
    time_started = models.DateTimeField(auto_now_add=True)
    time_end = models.DateTimeField(blank=True, null=True)
    task_status = models.CharField(blank=True,max_length=100)

    def clean(self):
        is_new = True if not self.id else False
        if is_new:
            user_incomplete_task= self.__class__.objects.filter(user=self.user).filter(task_status="PROCESSING").exists()

            if user_incomplete_task:
                from celery.result import AsyncResult
                user_incomplete_task= self.__class__.objects.filter(user=self.user).filter(task_status="PROCESSING").get()
                task_status = AsyncResult(user_incomplete_task.task_id)
                if task_status.state not in ['PROCESSING', 'PENDING', 'STARTED']:
                    user_incomplete_task.task_status = 'PROCESSED'
                    user_incomplete_task.time_end = timezone.now()
                    user_incomplete_task.save()
                else:
                    raise ValidationError(_('One Task is under processing/pending please cancel or wait'))

    def save(self, *args, **kwargs):
        is_new = True if not self.id else False
        if is_new:
            from .tasks import baseline_tasks
            task = baseline_tasks.delay()
            self.task_id = task.task_id
            self.task_status = 'PROCESSING'
        else:
            if self.task_status == 'CANCEL':
                from celery.task.control import revoke
                revoke(self.task_id, terminate=True)
                self.time_end = timezone.now()
        return super(BaselineUpload,self).save(*args, **kwargs)

    def __str__(self):
        return self.user.username + self.name + str(self.time_started)
