from django.contrib import admin

# Register your models here.
from .models import BaselineUpload


class BaselineUploadAdmin(admin.ModelAdmin):
    list_display = ["name","user","task_status", "time_started", "time_end"]

admin.site.register(BaselineUpload,BaselineUploadAdmin)

admin.site.site_header = "Collect Admin"
admin.site.site_title = "Collect Dashboard"
admin.site.index_title = "Welcome to Collect Dashboard"
