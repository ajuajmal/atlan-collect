from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.conf import settings
import requests
from atlan_collect.collect.models import BaselineUpload
from django.utils import timezone

class BaselineUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaselineUpload
        fields = ["name","description","file"]
        read_only_fields = ('task_id', 'task_status', 'time_started','user')

    def validate(self, data):
        print(self.context['request'].user)
        errors = dict()
        user = self.context['request'].user
        user_incomplete_task= BaselineUpload.objects.filter(user=user).filter(task_status="PROCESSING").exists()
        if  user_incomplete_task:
            from celery.result import AsyncResult
            user_incomplete_task = BaselineUpload.objects.filter(user=user).filter(task_status="PROCESSING").get()
            task_status = AsyncResult(user_incomplete_task.task_id)
            if task_status.state not in ['PROCESSING', 'PENDING', 'STARTED']:
                user_incomplete_task.task_status = 'PROCESSED'
                user_incomplete_task.time_end = timezone.now()
                user_incomplete_task.save()
            else:
                raise serializers.ValidationError({
                'task':'Your Previous Task is Still in Process , Please Cancel or Wait for Completion',
                'processing_task_id':user_incomplete_task.task_id,
                'task_status':user_incomplete_task.task_status,
                'task_starting_time':user_incomplete_task.time_started
                    })
        return data

    def create(self,validated_data):
        validated_data['user'] = self.context['request'].user
        user_task = super(BaselineUploadSerializer, self).create(validated_data)
        return user_task

class BaselineUploadTaskSerializer(serializers.Serializer):
    task_id = serializers.CharField()
    def validate(self, data):
        task_id = data.get('task_id')
        user = self.context['request'].user
        user_task= BaselineUpload.objects.filter(user=user).filter(task_id=task_id).exists()
        if not user_task:
            raise serializers.ValidationError({
            'task':'Invalid Task ID'
                })
        return data
    def save(self):
        task_id = self.validated_data['task_id']
