from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework import status
from django.shortcuts import get_object_or_404

from django.utils import timezone
from django.conf import settings
from rest_framework_simplejwt.tokens import RefreshToken
from atlan_collect.collect.models import BaselineUpload
from .serializers import BaselineUploadSerializer,BaselineUploadTaskSerializer

class BaselineUploadAPIView(CreateAPIView):
    serializer_class = BaselineUploadSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # We create a token than will be used for future auth
        user_task=serializer.instance
        create_message = {"message": "Your task is processing.",
            "task_id": user_task.task_id,
            "task_status":user_task.task_status
            }
        return Response(
            {**serializer.data, **create_message},
            status=status.HTTP_201_CREATED,
            headers=headers
        )

class BaselineUploadTaskStatusAPI(CreateAPIView):
    serializer_class = BaselineUploadTaskSerializer

    def get(self,request, *args, **kwargs):
        serializer= self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        from celery.result import AsyncResult
        user_task = BaselineUpload.objects.get(task_id=serializer.validated_data['task_id'])
        task_status = AsyncResult(user_task.task_id)
        return Response({**serializer.data, 'task_id': user_task.task_id,'task_status': user_task.task_status , 'start_time': user_task.time_started},status=status.HTTP_200_OK)
    def post(self,request, *args, **kwargs):
        return self.get(self.request)

class BaselineUploadTaskCancelAPI(CreateAPIView):
    serializer_class = BaselineUploadTaskSerializer

    def post(self,request, *args, **kwargs):
        serializer= self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        from celery.result import AsyncResult
        user_task = BaselineUpload.objects.get(task_id=serializer.validated_data['task_id'])
        task_status = AsyncResult(user_task.task_id)
        if task_status.state in ['PROCESSING', 'PENDING', 'STARTED']:
            from celery.task.control import revoke
            revoke(user_task.task_id, terminate=True)
            user_task.task_status = 'CANCELD'
            user_task.time_end = timezone.now()
            user_task.save()
            return Response({'task_id': user_task.task_id,'task_status': user_task.task_status , 'start_time': user_task.time_started},status=status.HTTP_200_OK)
        else:
            return Response({'status':'already PROCESSED', 'task_id': user_task.task_id,'task_status': user_task.task_status , 'start_time': user_task.time_started, 'end time':user_task.time_end },status=status.HTTP_200_OK)
