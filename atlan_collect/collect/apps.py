from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

class CollectConfig(AppConfig):
    name = 'atlan_collect.collect'
    verbose_name = _("Collect")

    def ready(self):
        try:
            import atlan_collect.feeds.signals  # noqa F401
        except ImportError:
            pass
