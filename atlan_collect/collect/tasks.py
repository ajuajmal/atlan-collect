from django.contrib.auth import get_user_model
from celery.exceptions import SoftTimeLimitExceeded,TimeoutError
from .models import BaselineUpload
from config import celery_app
import time
from django.utils import timezone
import pandas as pd
from django.shortcuts import get_object_or_404
import os
from django.core import serializers
import json

User = get_user_model()


@celery_app.task(name="baseline uploads",bind=True,time_limit=55555,soft_time_limit=55555)
def baseline_tasks(self):
    #print(celery_app.current_task.task_id)
    try:
        print('Executing task id {0.id}, args: {0.args!r} kwargs: {0.kwargs!r}'.format(
            self.request))
        while not BaselineUpload.objects.filter(task_id=self.request.id).exists():
            pass
        file = BaselineUpload.objects.get(task_id=self.request.id).file
        csv_file = file.file.open()
        dataframe = pd.read_csv(csv_file)
        csv_file.close()
        for index,row in dataframe.iterrows():
            time.sleep(1)
        task = get_object_or_404(BaselineUpload,task_id=self.request.id)
        task.task_status = 'SUCCESS'
        task.time_end = timezone.now()
        task.save()
        return "PROCESSED" + str(index) + "records"
    except:
        task = get_object_or_404(BaselineUpload,task_id=self.request.id)
        task.task_status = 'PROCESS TERMINATED'
        task.time_end = timezone.now()
        task.save()
        return "PROCESS TERMINATED"
