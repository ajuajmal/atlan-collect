from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter
from rest_framework_simplejwt import views as jwt_views
from django.urls import path

from atlan_collect.users.api.views import UserViewSet
from atlan_collect.collect.api.views import BaselineUploadAPIView,BaselineUploadTaskStatusAPI,BaselineUploadTaskCancelAPI

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)

urlpatterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('baseline/upload/', BaselineUploadAPIView.as_view(), name= 'baseline_uploads'),
    path('baseline/status/', BaselineUploadTaskStatusAPI.as_view(), name= 'baseline_uploads_status'),
    path('baseline/cancel/', BaselineUploadTaskCancelAPI.as_view(), name= 'baseline_uploads_cancel')
]

app_name = "api"
urlpatterns += router.urls
